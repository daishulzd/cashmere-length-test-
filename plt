import numpy as np
import matplotlib.pyplot as plt

x=np.arange(0,100,5) #生成0到100的一维数组,步进值为5
y=np.linspace(20,35,20) #生成20到35的一维数组,总共20个值,呈线性关系,未来应替代为手牌长度的值.

# print x
# print y

plt.figure(figsize=(8,4))
plt.plot(x,y,label="$sin(x)$",color="red",linewidth=2) 
#绘制曲线
plt.xlabel("Time(s)") #设置x轴标签
plt.ylabel("Volt") #设置y轴标签
plt.title("手排长度") #设置图表标题
plt.ylim(0,35) #设置y轴的上下限
plt.legend()
plt.show()